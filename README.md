# OmegaT flat theme plugin

This is OmegaT plugin to provide themes for OmegaT 5.6.0 and later.

## Themes

There are 4 basic themes and many variations from IntelliJ themes.

### Basic themes

|      name |   dark |
|-----------|-------:|
| Flat Dark |      v |
| Flat Light |        |
| Flat round dark |      v |
| Flat round light |        |


### IntteliJ themes 

These themes are derived from FlatLaf IntelliJ Themes Pack.

| name                    |   dark |
|-------------------------|-------:|
| Arc                     | |
| Arc Orange              | |
| Arc Dark                | v |
| Arc Orange Dark         | v |
| Carbon                  | v |
| Cobalt 2                | v |
| Cyan light              |  |
| Dark Flat               | v |
| Dark purple             | v |
| Dracula                 |  v |
| Gradianto Dark Fuchsia  | v |
| Gradianto Deep Ocean    | v |
| Gradianto Midnight Blue | v |
| Gradianto Nature Green  |  v |
| Gray                    | v |
| Hiberbee Dark           | v |
| High contrast           | v |
| Light Flat              |   |
| Material Design Dark    |  v |
| Monocai                 |  v  |
| Monocai Pro             | v  |
| Nord                    |  v  |
| One Dark                |   v  |
| Solarized Dark          | v |
| Spacegray               | v |
| Vuesion                 | v |
| Xcode-Dark              |  v |




## Dependency

OmegaT round theme plugin depends on [FlatLaf -Flat Look and Feel](https://github.com/JFormDesigner/FlatLaf)
by [FormDev Software](https://www.formdev.com/flatlaf/).

## License

The plugin is distributed under GNU General Public License 3 or later.

