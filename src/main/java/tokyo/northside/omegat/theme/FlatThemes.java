/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2021 Hiroshi Miura

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package tokyo.northside.omegat.theme;

import javax.swing.UIManager;

/**
 * OmegaT driver module to register themes.
 */
public final class FlatThemes {

    private FlatThemes() {
    }

    private static final String BASE_PATH = "tokyo.northside.omegat.theme.";

    private static final String[] THEMES = {
            "Flat Dark", "FlatDarkTheme",
            "Flat Light", "FlatLightTheme",
            "Flat Round Dark", "FlatRoundDarkTheme",
            "Flat Round Light", "FlatRoundLightTheme",
            "Arc", "FlatArcTheme",
            "Arc - Orange", "FlatArcOrangeTheme",
            "Carbon", "FlatCarbonTheme",
            "Cobalt2", "FlatCobalt2Theme",
            "Cyan light", "FlatCyanLightTheme",
            "Dark Flat", "FlatDarkFlatTheme",
            "Dark purple", "FlatDarkPurpleTheme",
            "Dracula", "FlatDraculaTheme",
            "Gradianto Dark Fuchsia", "FlatGradiantoDarkFuchsiaTheme",
            "Gradianto Deep Ocean", "FlatGradiantoDeepOceanTheme",
            "Gradianto Midnight Blue", "FlatGradiantoMidnightBlueTheme",
            "Gradianto Nature Green", "FlatGradiantoNatureGreenTheme",
            "Gray", "FlatGrayTheme",
            "High Contrast", "FlatHighContrastTheme",
            "Material Design Dark", "FlatMaterialDesignDarkTheme",
            "Monocai", "FlatMonocaiTheme",
            "Monocai Pro", "FlatMonocaiProTheme",
            "Nord", "FlatNordTheme",
            "One Dark", "FlatOneDarkTheme",
            "Solarized Dark", "FlarSolarizedDarkTheme",
            "Spacegray", "FlatSpacegrayTheme",
            "Vuesion", "FlatVuesionTheme",
            "Xcode Dark", "FlatXCodeDarkTheme",
    };

    /**
     * Register the plugin.
     */
    public static void loadPlugins() {
        // register themes as custom LaFs
        for (int i = 0; i < THEMES.length; i += 2) {
            UIManager.installLookAndFeel(THEMES[i], BASE_PATH + THEMES[i + 1]);
        }
    }

    /**
     * Unload plugins.
     * Currently, do nothing.
     */
    public static void unloadPlugins() {
    }

}
