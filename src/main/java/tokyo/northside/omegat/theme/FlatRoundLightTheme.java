/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2021 Hiroshi Miura

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/
package tokyo.northside.omegat.theme;

import com.formdev.flatlaf.FlatLightLaf;
import org.omegat.gui.theme.DefaultFlatTheme;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

/**
 * Flat and round light theme for OmegaT.
 * This is based on FlatLaf's FlatLightLaf module.
 */
@SuppressWarnings("unused")
public class FlatRoundLightTheme extends BaseLightThemes {
    private static final String NAME = "Flat round light theme";
    private static final String ID = "FlatRoundLightTheme";
    private static final String DESCRIPTION = "Rounded theme customized from FlatLightLaf";

    /**
     * Constructor.
     */
    public FlatRoundLightTheme() {
        super(ID, new FlatLightLaf());
    }

    /**
     * Return default theme configurations.
     *
     * @return
     */
    @Override
    public UIDefaults getDefaults() {
        UIDefaults defaults = super.getDefaults();
        return FlatThemeUtils.getRoundShapes(defaults);
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
