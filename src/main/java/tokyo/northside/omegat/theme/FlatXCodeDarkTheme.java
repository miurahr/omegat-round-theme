/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2022 Hiroshi Miura

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/
package tokyo.northside.omegat.theme;

/**
 * @author Hiroshi Miura
 */
@SuppressWarnings("unused")
public class FlatXCodeDarkTheme extends BaseFlatDarkIJThemes {
    private static final String NAME = "Xcode Dark";
    private static final String ID = "FlatXcodeDarkTheme";
    private static final String DESCRIPTION = "A theme from Xcode Dark Intellij";

    public FlatXCodeDarkTheme() {
        super(ID, new com.formdev.flatlaf.intellijthemes.FlatXcodeDarkIJTheme().getTheme());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
