/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2021 Hiroshi Miura

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/
package tokyo.northside.omegat.theme;

import com.formdev.flatlaf.FlatDarkLaf;
import org.omegat.gui.theme.DefaultFlatTheme;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;


/**
 * Flat and round dark theme for OmegaT.
 * This is based on FlatLaf's FlatDarkLaf module.
 */
@SuppressWarnings("unused")
public class FlatRoundDarkTheme extends BaseDarkThemes {
    private static final String NAME = "Flat round dark theme";
    private static final String ID = "FlatRoundDarkTheme";
    private static final String DESCRIPTION = "Rounded theme customized from FlatDarkLaf";

    /**
     * Constructor.
     */
    public FlatRoundDarkTheme() {
        super(ID, new FlatDarkLaf());
    }

    /**
     * Return default theme configurations.
     *
     * @return
     */
    @Override
    public UIDefaults getDefaults() {
        UIDefaults defaults = super.getDefaults();
        return FlatThemeUtils.getRoundShapes(defaults);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
