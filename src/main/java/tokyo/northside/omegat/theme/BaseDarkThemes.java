/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2021-2022 Hiroshi Miura

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package tokyo.northside.omegat.theme;

import com.formdev.flatlaf.FlatLaf;
import org.omegat.gui.theme.DefaultFlatTheme;

import javax.swing.*;
import javax.swing.border.MatteBorder;


/**
 * @author Hiroshi Miura
 */
@SuppressWarnings("unused")
public abstract class BaseDarkThemes extends FlatLaf {
    private final String id;
    private final LookAndFeel parent;

    public BaseDarkThemes(String id, LookAndFeel parent) {
        super();
        this.id = id;
        this.parent = parent;
    }

    /**
     * Return default theme configurations.
     */
    @Override
    public UIDefaults getDefaults() {
        UIDefaults original = parent.getDefaults();
        UIDefaults defaults = DefaultFlatTheme.setDefaults(original, id);  // get OmegaT defaults
        UIDefaults custom = FlatThemeUtils.setDarkDefaults(defaults);
        UIManager.put("DockViewTitleBar.border", new MatteBorder(1, 1, 1, 1, custom.getColor("border")));
        FlatThemeUtils.setupDecoration();
        return custom;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public boolean isDark() {
        return true;
    }
}
