/**************************************************************************
 OmegaT - Computer Assisted Translation (CAT) tool
          with fuzzy matching, translation memory, keyword search,
          glossaries, and translation leveraging into updated projects.

 Copyright (C) 2022 Hiroshi Miura

 OmegaT is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 OmegaT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

package tokyo.northside.omegat.theme;


import com.formdev.flatlaf.IntelliJTheme;
import org.omegat.gui.theme.DefaultFlatTheme;

import javax.swing.*;
import javax.swing.border.MatteBorder;

/**
 * @author Hiroshi Miura
 */
abstract class BaseFlatLightIJThemes extends IntelliJTheme.ThemeLaf {

    protected String id;

    public BaseFlatLightIJThemes(String id, IntelliJTheme intelliJTheme) {
        super(intelliJTheme);
        this.id = id;
    }

    public String getID() {
        return id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UIDefaults getDefaults() {
        UIDefaults flatLafDefaults = super.getDefaults();
        UIDefaults defaults = DefaultFlatTheme.setDefaults(flatLafDefaults, id); // get omegat defaults
        UIDefaults custom = FlatThemeUtils.setLightDefaults(defaults);
        UIManager.put("DockViewTitleBar.border", new MatteBorder(1, 1, 1, 1, custom.getColor("borderColor")));
        FlatThemeUtils.setupDecoration();
        return custom;
    }

    @Override
    public boolean isNativeLookAndFeel() {
        return false;
    }

    @Override
    public boolean isSupportedLookAndFeel() {
        return true;
    }

    public boolean isDark() {
        return false;
    }
}
