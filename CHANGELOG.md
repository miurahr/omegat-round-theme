# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [v0.3.1]

* Fix float dockable window decoration problem.
  A previous version can be closed but cannot reopen.
* Drop Solarized light theme.

## [v0.3.0]

* Add many themes from FlatLaf IntelliJ Themes Pack.
* Design licenses are in lib/licenses folder. 
* Introduce Base Theme class to reduce source code duplication.

## [v0.2.1]

* Use embed title bar on linux
* Fix fatal error in MANIFEST.MF when loading plugins (#11)
* Bump FlatLaF@v2.6

## [v0.2.0]

* Update README
* Forked from OmegaT-round-theme.
* Add original flatlaf design themes.

## [v0.1.0]

### Added
* Set application color style

### Changed
* Automate release with CI
* Bump versions
    * gradle omegat plugin@v1.5.7
* Use git tag for release versioning

[Unreleased]: https://codeberg.org/miurahr/omegat-flat-theme/compare/v0.3.1...HEAD
[v0.3.1]: https://codeberg.org/miurahr/omegat-flat-theme/compare/v0.3.0...v0.3.1
[v0.3.0]: https://codeberg.org/miurahr/omegat-flat-theme/compare/v0.2.1...v0.3.0
[v0.2.1]: https://codeberg.org/miurahr/omegat-flat-theme/compare/v0.2.0...v0.2.1
[v0.2.0]: https://codeberg.org/miurahr/omegat-flat-theme/compare/v0.1.0...v0.2.0
[v0.1.0]: https://codeberg.org/miurahr/omegat-flat-theme/compare/v0.0.1...v0.1.0
